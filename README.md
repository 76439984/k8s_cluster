# k8s_cluster

#### 介绍
二进制部署的k8s 1.15版本,包含容器集群的持续交付CICD、ELK等等生态...陆续更新后面的  
使用二进制部署的原因是方便维护,部署的过程可以更加了解k8s的架构原理等。另一方面是，kubeadm部署的集群证书签发只有1年，虽然目前已经可以使用kubeadm来续期或者安装前修改源码来实现续期跟修改时长。但是需要注意的是kubeadm续期的证书只会对当时初始化集群签发的证书进行续期，不会给后来集群部署手动签发的证书进行续期，这就有可能出现不必要的麻烦。所以，在二进制部署的情况下，可以对证书签发时间修改成更长的时间，大大避免了证书过期带来的问题。  

由于一直使用印象笔记来记录工作遇到事情,因此所以文档都将以印象笔记的形式给出超链接方式分享.  
同时,也给出另一个库 [#####常用配置资源文件清单注释#####](https://gitee.com/76439984/k8s.git)
#### 软件架构
![架构图](https://images.gitee.com/uploads/images/2020/0516/180903_ada206bb_37028.jpeg "1.jpg")
[kubernetes 1.15.2高可用集群(一) 架构图与服务器基础环境初始化](https://www.yinxiang.com/everhub/note/0e24621c-3a78-4332-adbf-8c2f8f9c4056)

#### 安装教程
[kubernetes 1.15.2高可用集群(二) bind9+NamedManager 主辅DNS安装部署](https://www.yinxiang.com/everhub/note/99402de9-ce6d-44c3-9b25-1bb90aeb1458)  
[kubernetes 1.15.2高可用集群(二) bind9主辅DNS安装部署](https://www.yinxiang.com/everhub/note/4c3eb88a-a2ac-4f03-9ecc-6906419bb894)  
[kubernetes 1.15.2高可用集群(三) DEVOPS环境----集群CA证书生成	](https://www.yinxiang.com/everhub/note/8d4c3fb6-ec63-459e-9216-75832016cc9e)  
[kubernetes 1.15.2高可用集群(四) DEVOPS环境----harbor镜像仓库	](https://www.yinxiang.com/everhub/note/0b87d2f3-ff04-4e98-ba69-807fd7df6b92)  
[kubernetes 1.15.2高可用集群(五) 集群配置数据库ETCD部署	](https://www.yinxiang.com/everhub/note/9ace68da-8b7c-4905-a7a9-9983c8d51b0e)  
[kubernetes 1.15.2高可用集群(六) kube-apiserver组件安装	](https://www.yinxiang.com/everhub/note/7f3289ee-fc2e-4f7b-a5fd-21ed85cb591d)  
[kubernetes 1.15.2高可用集群(七) 部署kube-apiserver入口负载均衡	](https://www.yinxiang.com/everhub/note/60f39203-7e8c-440b-8224-cd1885fb563e)  
[kubernetes 1.15.2高可用集群(八) kube-controller-manager组件安装	](https://www.yinxiang.com/everhub/note/a1bdf9a7-b060-444e-8da0-863e2d45ea55)  
[kubernetes 1.15.2高可用集群(九) kube-scheduler组件安装	](https://www.yinxiang.com/everhub/note/679b7dd5-9466-4aa3-ae6c-648311606871)  
[kubernetes 1.15.2高可用集群(十) kubelet组件安装	](https://www.yinxiang.com/everhub/note/600a010a-e34f-43da-bbb1-f01696533645)  
[kubernetes 1.15.2高可用集群(十一) kube-proxy组件安装	](https://www.yinxiang.com/everhub/note/4bbc4b8c-a083-4a38-8169-0a50c23a37a2)  
[kubernetes 1.15.2高可用集群(十二) 核心网络插件Flannel安装	](https://www.yinxiang.com/everhub/note/0aae8f65-7076-4775-aaf6-1e8a4530849b)  
[kubernetes 1.15.2高可用集群(十三) 服务发现-coredns	](https://www.yinxiang.com/everhub/note/ff38d30c-5e1d-432f-b218-0db7cc162721)  
[kubernetes 1.15.2高可用集群(十四) 添加删除node节点	](https://www.yinxiang.com/everhub/note/22eca7aa-9847-477d-b0d3-2378f5cf7eda)  
[kubernetes 1.15.2高可用集群(十五) 服务暴露-ingress控制器之traefik	](https://www.yinxiang.com/everhub/note/b4145ebc-6d5b-452c-8e3d-65e353077a24)  
[kubernetes 1.15.2高可用集群(十六) 创建kubectl用来连接集群apiserver的config文件	](https://www.yinxiang.com/everhub/note/f6c8b97a-928f-43e9-921c-57f7cba67dee)  
[kubernetes 1.15.2高可用集群(十七) dashboard v1.8.3（免认证）--可视化管理平台	](https://www.yinxiang.com/everhub/note/f7ac489b-f24d-4cea-a5a3-a9cee9ca4c0a)  
[kubernetes 1.15.2高可用集群(十七) dashboard v1.10.1（认证）--可视化管理平台	](https://www.yinxiang.com/everhub/note/af7f1887-8c15-4fad-a651-fa7382b16cc7)  
[kubernetes的CICD(一) 环境准备----jenkins容器部署	](https://www.yinxiang.com/everhub/note/dae2dc9d-4c6d-4046-83cc-5e5b0dfcc99c)  
[kubernetes的CICD(二) 环境准备----集成多版本maven+jdk到jenkins平台	](https://www.yinxiang.com/everhub/note/73c471ad-3b65-4fda-bca4-bfb0d07cdee2)  
[kubernetes的CICD(三) 环境准备----java底包镜像制作	](https://www.yinxiang.com/everhub/note/7cefb302-8c79-4376-81b4-bae7ff677735)  
[kubernetes的CICD(四) 环境准备----部署jenkins流水线	](https://www.yinxiang.com/everhub/note/8f3a6f2a-c51f-4603-ba60-f85049de0875)  
[交付dubbo服务到kubernetes(一) 环境准备----zookeeper	](https://www.yinxiang.com/everhub/note/3dafc0d0-7e1d-4ea9-ae2d-b202e4513750)  
[交付dubbo服务到kubernetes(二) 测试构建交付----dubbo服务器提供者	](https://www.yinxiang.com/everhub/note/5f9eee89-a8f8-4dbc-8c27-fe30371a3613)  
[交付dubbo服务到kubernetes(三) 测试构建交付----dubbo-monitor监控服务	](https://www.yinxiang.com/everhub/note/b38971e6-da87-4042-b8bf-3d681467a3dd)  
[交付dubbo服务到kubernetes(四) 测试构建交付----dubbo服务器消费者	](https://www.yinxiang.com/everhub/note/e9e9bd48-f898-4c11-b778-2d437da69256)  
[交付dubbo服务到kubernetes(五) configmap配置中心交付dubbo-monitor----环境准备测试环境zookeeper	](https://www.yinxiang.com/everhub/note/8b678477-5989-48ce-9563-746e1257c85e)  
[交付dubbo服务到kubernetes(六) configmap配置中心交付dubbo-monitor----测试环境dubbo-monitor	](https://www.yinxiang.com/everhub/note/563263a0-c3f0-4f79-b20f-586568fe69c0)  
[交付dubbo服务到kubernetes(七) 单一环境交付apollo配置中心	](https://www.yinxiang.com/everhub/note/f8c06f94-0783-42e7-a645-c1e26aac5adc)  
[交付dubbo服务到kubernetes(八) 单一环境apollo配置中心使用	](https://www.yinxiang.com/everhub/note/3afe7799-478e-4821-8cd5-288415c87523)  
[交付dubbo服务到kubernetes(九) 多环境交付apollo配置中心	](https://www.yinxiang.com/everhub/note/15bde8fe-57dd-4011-9753-6c0dda6d4480)  
[交付dubbo服务到kubernetes(十) 多环境apollo配置中心使用	](https://www.yinxiang.com/everhub/note/e696998b-7236-4dfd-86a3-9d50022900bd)  
[交付dubbo服务到kubernetes(十一) 上线流程	](https://www.yinxiang.com/everhub/note/88c9b499-17f0-49a3-ae57-d11b77ea3428)  
[kubernetes监控体系(一) prometheus监控及grafana----常用的exporter	](https://www.yinxiang.com/everhub/note/87101d45-e43a-4c1c-8717-e181d65b3fbc)  
[kubernetes监控体系(二) prometheus监控及grafana----prometheus-server	](https://www.yinxiang.com/everhub/note/ecf9d935-11e7-4eeb-b47b-56366ac6d98e)  
[kubernetes监控体系(三) prometheus监控及grafana----grafana	](https://www.yinxiang.com/everhub/note/14803104-81d0-48a5-8d47-b63390e88958)  
[kubernetes监控体系(四) prometheus监控及grafana----alertmanager邮件告警	](https://www.yinxiang.com/everhub/note/48575e63-7854-4df3-a99a-565674512e19)  
[kubernetes日志收集(一) ELK架构----tomcat底包构建	](https://www.yinxiang.com/everhub/note/da57f670-9da3-4c53-8964-33778dfb4730)  
[kubernetes日志收集(二) ELK架构----二进制部署ES集群	](https://www.yinxiang.com/everhub/note/967594da-63d5-4859-a41a-d14d9a61e4be)  
[kubernetes日志收集(三) ELK架构----二进制部署Kafka集群	](https://www.yinxiang.com/everhub/note/6408f2a4-57fa-4f4b-b0a5-85d887c03ffc)  
[kubernetes日志收集(四) ELK架构----filebeat底包构建	](https://www.yinxiang.com/everhub/note/add46aee-2347-4f22-9149-7ac9134cf8e7)  
[kubernetes日志收集(五) ELK架构----部署logstash	](https://www.yinxiang.com/everhub/note/e2a806b7-a35b-4758-b98d-240f4d3678d2)  
[kubernetes日志收集(六) ELK架构----部署kibana	](https://www.yinxiang.com/everhub/note/c2f79360-379d-432c-91a7-4298bff12e53)  
[kubernetes HPA弹性伸缩(一) 指标采集----metrics资源指标采集	](https://www.yinxiang.com/everhub/note/05dc8dbd-81e8-4067-aa75-9e7b494a17be)  

#### 集群填坑
[kubernetes 1.15.2 交付高可用apollo配置中心](https://www.yinxiang.com/everhub/note/4477dee7-7bcc-4df8-b461-ec4f15acaef4)  
[harbor的双向同步配置](https://www.yinxiang.com/everhub/note/8f32e720-5372-4468-983e-ff055ffac0cb)  


#### 集群维护
[kubernetes集群维护(一) 集群节点下线](https://www.yinxiang.com/everhub/note/592583da-a9b5-47f0-8871-1d1c12b94c76)  
[kubernetes集群维护(二) etcd故障恢复](https://www.yinxiang.com/everhub/note/2d725665-0a53-4847-b880-e710f9013865)
